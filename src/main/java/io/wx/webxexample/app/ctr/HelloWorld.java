/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.webxexample.app.ctr;

import io.wx.core.HTTP.GET;
import io.wx.core.HTTP.HttpController;
import io.wx.core.HTTP.Resource;
import io.wx.core.cdi.l4j.InjectLogger;
import javax.inject.Inject;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Resource(path = "/hello")
public class HelloWorld {
    @Inject HttpController ctr;
    @InjectLogger Logger logger;
    
    @GET
    public String hw(){
        String greet =  "hello " + ( ( ctr.getRequest().params().get("u") != null) ?  ctr.getRequest().params().get("u") : "world ");
        logger.info(greet);
        return greet;
    }
    
}
