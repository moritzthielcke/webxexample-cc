package io.wx.webxexample;
import io.wx.core.HTTP.impl.AbstractApplication;
import io.wx.core.config.CloudConfigurator;
import io.wx.core.config.SimpleConfig;


/**
 *
 * @author moritz
 */
public class ExampleApplication extends AbstractApplication{

    public static void main(String[] args) throws Exception {
        new ExampleApplication().start().idle();
    }
    
    public ExampleApplication() throws Exception{
        super(new CloudConfigurator(
            new SimpleConfig().setPort(1337).setInstanceCount(3).setSSL(true)
        ));
    }
    
    @Override
    public String getApplicationPath() {
        return "io.wx.webxexample.app";
    }
    
}
